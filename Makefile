ll: bin/utilfechaestatico  bin/utilfechadinamico

bin/utilfechaestatico: include/libutilfechaestatico.a
		gcc -static obj/main.o -lutilfechaestatico -L include/  -o bin/utilfechaestatico

bin/utilfechadinamico: include/libutilfechadinamico.so
		gcc obj/main.o -lutilfechadinamico -L include/ -o bin/utilfechadinamico

include/libutilfechaestatico.a: obj/main.o obj/dias.o obj/segundos.o obj/formato.o
		ar rcs include/libutilfechaestatico.a obj/dias.o obj/segundos.o	obj/formato.o

include/libutilfechadinamico.so: obj/main.o obj/dias.o obj/segundos.o obj/formato.o
		gcc -shared -fPIC -o include/libutilfechadinamico.so src/dias.c  src/segundos.c src/formato.c


obj/main.o: src/main.c
		gcc -c src/main.c -I include/ -o obj/main.o


obj/segundos.o: src/segundos.c
		gcc -c src/segundos.c -o obj/segundos.o

obj/dias.o: src/dias.c
		gcc -c src/dias.c -o obj/dias.o

obj/formato.o: src/formato.c
		gcc -c src/formato.c -o obj/formato.o
clean:
		rm -f bin/utilfecha obj/*.o *.o include/libutilfechaestatico.a include/libutilfechadinamico.so bin/utilfechaestatico bin/utilfechadinamicoo 
